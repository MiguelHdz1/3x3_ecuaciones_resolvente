package sist.ecu.mx;

import java.awt.Color;

/**
 *
 * @author luismiguel
 */
public class Ecuacuiones extends javax.swing.JFrame {

    public Ecuacuiones() {
        initComponents();
        this.getContentPane().setBackground(Color.DARK_GRAY);

    }

    // public static double [][] array = new double[3][3];

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        txt1 = new javax.swing.JTextField();
        txt11 = new javax.swing.JTextField();
        txt111 = new javax.swing.JTextField();
        txt2 = new javax.swing.JTextField();
        txt22 = new javax.swing.JTextField();
        txt222 = new javax.swing.JTextField();
        txt3 = new javax.swing.JTextField();
        txt33 = new javax.swing.JTextField();
        txt333 = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        btnResolver = new javax.swing.JButton();
        txtMostrar = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        txtResut1 = new javax.swing.JTextField();
        txtResult2 = new javax.swing.JTextField();
        txtResult3 = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        txtMX = new javax.swing.JTextField();
        txtMY = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        txtMZ = new javax.swing.JTextField();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        txtFx = new javax.swing.JTextField();
        txtFy = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        txtFZ = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Ecuaciones 3x3 PERROMOJADO");
        setAutoRequestFocus(false);
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        txt111.setCursor(new java.awt.Cursor(java.awt.Cursor.TEXT_CURSOR));

        jLabel1.setForeground(new java.awt.Color(255, 250, 250));
        jLabel1.setText("x");

        jLabel2.setForeground(new java.awt.Color(255, 250, 250));
        jLabel2.setText("x");

        jLabel3.setForeground(new java.awt.Color(255, 250, 250));
        jLabel3.setText("x");

        jLabel4.setForeground(new java.awt.Color(255, 250, 250));
        jLabel4.setText("y");

        jLabel5.setForeground(new java.awt.Color(255, 250, 250));
        jLabel5.setText("y");

        jLabel6.setForeground(new java.awt.Color(255, 250, 250));
        jLabel6.setText("y");

        jLabel7.setForeground(new java.awt.Color(255, 250, 250));
        jLabel7.setText("z");

        jLabel8.setForeground(new java.awt.Color(255, 250, 250));
        jLabel8.setText("z");

        jLabel9.setForeground(new java.awt.Color(255, 250, 250));
        jLabel9.setText("z");

        btnResolver.setText("Resolver");
        btnResolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnResolverActionPerformed(evt);
            }
        });

        jLabel10.setForeground(new java.awt.Color(255, 250, 250));
        jLabel10.setText("=");

        jLabel11.setForeground(new java.awt.Color(255, 250, 250));
        jLabel11.setText("=");

        jLabel12.setForeground(new java.awt.Color(255, 250, 250));
        jLabel12.setText("=");

        jLabel13.setForeground(new java.awt.Color(255, 250, 250));
        jLabel13.setText("Determinante delta");

        jLabel14.setForeground(new java.awt.Color(255, 250, 250));
        jLabel14.setText("Determinante de x");

        jLabel15.setForeground(new java.awt.Color(255, 250, 250));
        jLabel15.setText("Determinante de Y");

        jLabel16.setForeground(new java.awt.Color(255, 250, 250));
        jLabel16.setText("Determinante de z");

        jLabel17.setForeground(new java.awt.Color(255, 250, 250));
        jLabel17.setText("x=");

        jLabel18.setForeground(new java.awt.Color(255, 250, 250));
        jLabel18.setText("y=");

        jLabel19.setForeground(new java.awt.Color(255, 250, 250));
        jLabel19.setText("z=");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txt1, javax.swing.GroupLayout.DEFAULT_SIZE, 38, Short.MAX_VALUE)
                            .addComponent(txt2)
                            .addComponent(txt3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txt11, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(txt111, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel3)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(txt33, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addGroup(layout.createSequentialGroup()
                                        .addComponent(jLabel2)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(txt22, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(0, 0, Short.MAX_VALUE)))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addComponent(jLabel5)
                                        .addGap(26, 26, 26)
                                        .addComponent(txt222, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 13, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(txt333, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel10)
                                .addGap(18, 18, 18)
                                .addComponent(txtResut1, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel9)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel12)
                                .addGap(18, 18, 18)
                                .addComponent(txtResult3))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel8)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel11)
                                .addGap(18, 18, 18)
                                .addComponent(txtResult2)))
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel17)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtFx, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel18)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtFy, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addComponent(txtMostrar)
                                    .addComponent(jLabel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(31, 31, 31)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel14)
                                            .addComponent(btnResolver)))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGap(18, 18, 18)
                                        .addComponent(txtMX, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel19)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtFZ, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(54, 54, 54))
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel15)
                                    .addComponent(txtMY, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addComponent(jLabel16)
                                        .addGap(19, 19, 19))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addComponent(txtMZ, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addContainerGap())))))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(79, 79, 79)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt111, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel4)
                    .addComponent(jLabel7)
                    .addComponent(jLabel10)
                    .addComponent(txtResut1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(39, 39, 39)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt222, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel5)
                    .addComponent(jLabel8)
                    .addComponent(jLabel11)
                    .addComponent(txtResult2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(43, 43, 43)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt33, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt333, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(jLabel6)
                    .addComponent(jLabel9)
                    .addComponent(jLabel12)
                    .addComponent(txtResult3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(42, 42, 42)
                .addComponent(btnResolver)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel13)
                    .addComponent(jLabel14)
                    .addComponent(jLabel15)
                    .addComponent(jLabel16))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtMostrar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtMX, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtMY, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtMZ, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(42, 42, 42)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel17)
                    .addComponent(txtFx, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtFy, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel18)
                    .addComponent(jLabel19)
                    .addComponent(txtFZ, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(42, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnResolverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnResolverActionPerformed
        // TODO add your handling code here:

        double[] arreglo = new double[12];

        arreglo[0] = Double.parseDouble(txt1.getText());
        arreglo[1] = Double.parseDouble(txt11.getText());
        arreglo[2] = Double.parseDouble(txt111.getText());
        arreglo[3] = Double.parseDouble(txt2.getText());
        arreglo[4] = Double.parseDouble(txt22.getText());
        arreglo[5] = Double.parseDouble(txt222.getText());
        arreglo[6] = Double.parseDouble(txt3.getText());
        arreglo[7] = Double.parseDouble(txt33.getText());
        arreglo[8] = Double.parseDouble(txt333.getText());
        arreglo[9] = Double.parseDouble(txtResut1.getText());
        arreglo[10] = Double.parseDouble(txtResult2.getText());
        arreglo[11] = Double.parseDouble(txtResult3.getText());

        //DETERMINANTE DELTA
        double diagonal1 = arreglo[0] * arreglo[4] * arreglo[8];
        double diagonal2 = arreglo[1] * arreglo[5] * arreglo[6];
        double diagonal3 = arreglo[2] * arreglo[3] * arreglo[7];
        double diangonal1neg = -1 * arreglo[6] * arreglo[4] * arreglo[2];
        double diagonal2neg = -1 * arreglo[7] * arreglo[5] * arreglo[0];
        double diagonal3neg = -1 * arreglo[8] * arreglo[3] * arreglo[1];

        //p es mi determinante delta 
        double DeterminanteDelta = diagonal1 + diagonal2 + diagonal3 + diangonal1neg
                + diagonal2neg + diagonal3neg;

        String DeterminanteD = Double.toString(DeterminanteDelta);
        txtMostrar.setText(DeterminanteD);

        //DETERMINANTE X 
        double diagonalx1 = arreglo[9] * arreglo[8] * arreglo[4];
        double diagonalx2 = arreglo[1] * arreglo[5] * arreglo[11];
        double diagonalx3 = arreglo[2] * arreglo[7] * arreglo[10];
        double diagonalx4 = -1 * arreglo[11] * arreglo[2] * arreglo[4];
        double diagonalx5 = -1 * arreglo[9] * arreglo[5] * arreglo[7];
        double diagonalx6 = -1 * arreglo[10] * arreglo[8] * arreglo[1];
        //aa es mi determinante de x 
        double DeterminanteDeX = diagonalx1 + diagonalx2 + diagonalx3
                + diagonalx4 + diagonalx5 + diagonalx6;
        String DeterminanteX = Double.toString(DeterminanteDeX);
        txtMX.setText(DeterminanteX);

        //DETERMINATE Y           
        double diagonaly1 = arreglo[0] * arreglo[10] * arreglo[8];
        double diagonaly2 = arreglo[9] * arreglo[5] * arreglo[6];
        double diagonaly3 = arreglo[2] * arreglo[3] * arreglo[11];
        double diagonaly4 = -1 * arreglo[6] * arreglo[2] * arreglo[10];
        double diagonaly5 = -1 * arreglo[11] * arreglo[5] * arreglo[0];
        double diagonaly6 = -1 * arreglo[9] * arreglo[8] * arreglo[3];
        //ai es mi determinante de y
        double DeterminanteDeY = diagonaly1 + diagonaly2 + diagonaly3
                + diagonaly4 + diagonaly5 + diagonaly6;

        String DeterminanteY = Double.toString(DeterminanteDeY);
        txtMY.setText(DeterminanteY);
        

        //DETERMINANTE DE Z
        double diagonalz1 = arreglo[0] * arreglo[11] * arreglo[4];
        double diagonalz2 = arreglo[1] * arreglo[10] * arreglo[6];
        double diagonalz3 = arreglo[9] * arreglo[7] * arreglo[3];
        double diagonalz4 = -1 * arreglo[6] * arreglo[9] * arreglo[4];
        double diagonalz5 = -1 * arreglo[0] * arreglo[10] * arreglo[7];
        double diagonalz6 = -1 * arreglo[11] * arreglo[3] * arreglo[1];

        //aq es mi determinante de z 
        double DeterminanteDeZ = diagonalz1 + diagonalz2 + diagonalz3 + diagonalz4
                + diagonalz5 + diagonalz6;
        String DeterminanteZ = Double.toString(DeterminanteDeZ);
        txtMZ.setText(DeterminanteZ);

        //CALCULO DE X   
        double ResultadoDeX = DeterminanteDeX / DeterminanteDelta;
        String ResutaldoX = Double.toString(ResultadoDeX);
        txtFx.setText(ResutaldoX);

        //CALCULO DE Y 
        double ResultadoDeY = DeterminanteDeY / DeterminanteDelta;
        String ResultadoY = Double.toString(ResultadoDeY);
        txtFy.setText(ResultadoY);

        //CALCULO DE Z
        double ResultadoDeZ = DeterminanteDeZ / DeterminanteDelta;
        String ResultadoZ = Double.toString(ResultadoDeZ);
        txtFZ.setText(ResultadoZ);


    }//GEN-LAST:event_btnResolverActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Ecuacuiones.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Ecuacuiones.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Ecuacuiones.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Ecuacuiones.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Ecuacuiones().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnResolver;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JTextField txt1;
    private javax.swing.JTextField txt11;
    private javax.swing.JTextField txt111;
    private javax.swing.JTextField txt2;
    private javax.swing.JTextField txt22;
    private javax.swing.JTextField txt222;
    private javax.swing.JTextField txt3;
    private javax.swing.JTextField txt33;
    private javax.swing.JTextField txt333;
    private javax.swing.JTextField txtFZ;
    private javax.swing.JTextField txtFx;
    private javax.swing.JTextField txtFy;
    private javax.swing.JTextField txtMX;
    private javax.swing.JTextField txtMY;
    private javax.swing.JTextField txtMZ;
    private javax.swing.JTextField txtMostrar;
    private javax.swing.JTextField txtResult2;
    private javax.swing.JTextField txtResult3;
    private javax.swing.JTextField txtResut1;
    // End of variables declaration//GEN-END:variables
}
